from django.shortcuts import redirect, render, get_object_or_404
from receipts.forms import ReceiptForm, ExpenseCategoryForm
from receipts.models import Account, ExpenseCategory, Receipt
from django.contrib.auth.decorators import login_required


# Create your views here.


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    objects = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "objects": objects,
        "title": "Categories",
    }
    return render(request, "receipts/detail.html", context)


@login_required
def account_list(request):
    objects = Account.objects.filter(owner=request.user)
    context = {
        "objects": objects,
        "title": "Accounts",
    }
    return render(request, "receipts/detail.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/ExpenseCategory.html", context)


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipts,
    }
    return render(request, "receipts/list.html", context)
